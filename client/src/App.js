import React, { useEffect, useState } from 'react';

import Home from './components/Home';
import Cart from './components/Cart';
import Navbar from './components/Navbar';
import LoadProduct from './components/LoadProduct';
import UpdateProduct from './components/UpdateProducts';
import { getProductsInCart } from './requests/cart';

import { 
  BrowserRouter as Router, 
  Switch, 
  Route, 
  Redirect 
} from 'react-router-dom';

import CartContext from './contexts/CartContext';

function App() {
  const [cart, setCart] = useState();
  const [state, setState] = useState(0);
  const [error, setError] = useState(null);

  const isAdmin = true;

  useEffect(() => {
    getProductsInCart()
    .then(data => {
      setState(1);
      setCart(data);
    })
    .catch(err => {
      setState(-1);
      setError(err.message);
    })
  }, [])

  return (
    <>
    { state === 1 ?
        <CartContext.Provider value={{cart,setCart}}>
          <Router>
            <Navbar isAdmin={isAdmin}/>
            <Switch>
              <Route exact path="/">
                <Home isAdmin={isAdmin}/>
              </Route>
              <Route exact path='/carrito'>
                <Cart isAdmin={isAdmin}/>
              </Route>
              <Route exact path='/productos/:id'>
                {isAdmin || <Redirect to="/"/>}
                <UpdateProduct/>
              </Route>
              <Route exact path='/productos'>
                {isAdmin || <Redirect to="/"/>}
                <LoadProduct/>
              </Route>
            </Switch>
          </Router>
        </CartContext.Provider>
      : 
        (state === 0 ? 
          <h1>Cargando </h1> 
        : 
          <h1>Error: {error}</h1>
        )
    }
    </>
  )
}

export default App;