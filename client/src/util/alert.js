export default function createAlert(elementRef, message, time, type) {
    const alert = document.createElement("div");
    alert.className = `alert alert-${type}`;
    alert.innerText = message;
    elementRef.current.appendChild(alert);
    setTimeout(() => {
        elementRef.current.removeChild(alert);
    }, time)
}