import { 
    GET_PRODUCTS,
    POST_PRODUCT,
    PUT_PRODUCT,
    DELETE_PRODUCT
} from './endpoints';

export function getProducts() { 
    return new Promise((resolve, reject) => {
        fetch(`${GET_PRODUCTS}`)
        .then(response => response.json())
        .then(data => {
            if(data.status === "success") resolve(data.data);
            else reject(new Error(data.message));
        })
        .catch(err => { reject(new Error(err.message)) })
    })
}

export function getProduct(id) { 
    return new Promise((resolve, reject) => {
        fetch(`${GET_PRODUCTS}/${id}`)
        .then(response => response.json())
        .then(data => {
            if(data.status === "success") resolve(data.data);
            else reject(new Error(data.message));
        })
        .catch(err => { reject(new Error(err.message)) })
    })
}

export function addProduct(data) { 
    return new Promise((resolve, reject) => {
        fetch(`${POST_PRODUCT}`, {method: "POST", body: data})
        .then(response => response.json())
        .then(data => {
            if(data.status === "success") resolve(data.data);
            else reject(new Error(data.message));
        })
        .catch(err => { reject(new Error(err.message)) })
    })
}

export function updateProduct(id, data) { 
    return new Promise((resolve, reject) => {
        fetch(`${PUT_PRODUCT}/${id}`, {method: "PUT", body: data})
        .then(response => response.json())
        .then(data => {
            if(data.status === "success") resolve(data.data);
            else reject(new Error(data.message));
        })
        .catch(err => { reject(new Error(err.message)) })
    })
}

export function deleteProduct(id) { 
    return new Promise((resolve, reject) => {
        fetch(`${DELETE_PRODUCT}/${id}`, {method: "DELETE"})
        .then(response => response.json())
        .then(data => {
                if(data.status === "success") resolve(data.data);
                else reject(new Error(data.message));
            })
            .catch(err => { reject(new Error(err.message)) })
    })
}
