const HOST_API = process.env.HOST_API || "http://localhost:8080";

export const GET_PRODUCTS = HOST_API + '/productos/listar';
export const POST_PRODUCT = HOST_API + '/productos/guardar';
export const PUT_PRODUCT = HOST_API + '/productos/actualizar';
export const DELETE_PRODUCT = HOST_API + '/productos/borrar';
export const GET_PRODUCTS_CART = HOST_API + '/carrito/listar';
export const POST_PRODUCT_CART = HOST_API + '/carrito/agregar';
export const DELETE_PRODUCT_CART = HOST_API + '/carrito/borrar';