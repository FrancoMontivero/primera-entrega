import { 
    GET_PRODUCTS_CART,
    POST_PRODUCT_CART,
    DELETE_PRODUCT_CART
} from './endpoints';

export function getProductsInCart() { 
    return new Promise((resolve, reject) => {
        fetch(`${GET_PRODUCTS_CART}`)
        .then(response => response.json())
        .then(data => {
            if(data.status === "success") resolve(data.data);
            else reject(new Error(data.message));
        })
        .catch(err => { reject(new Error(err.message)) })
    })
}


export function getProductInCart(id) { 
    return new Promise((resolve, reject) => {
        fetch(`${GET_PRODUCTS_CART}/${id}`)
        .then(response => response.json())
        .then(data => {
            if(data.status === "success") resolve(data.data);
            else reject(new Error(data.message));
        })
        .catch(err => { reject(new Error(err.message)) })
    })
}

export function addProductInCart(id) { 
    return new Promise((resolve, reject) => {
        fetch(`${POST_PRODUCT_CART}/${id}`, {method: "POST"})
        .then(response => response.json())
        .then(data => {
            if(data.status === "success") resolve(data.data);
            else reject(new Error(data.message));
        })
        .catch(err => { reject(new Error(err.message)) })
    })
}

export function deleteProductInCart(id) { 
    return new Promise((resolve, reject) => {
        fetch(`${DELETE_PRODUCT_CART}/${id}`, {method: "DELETE"})
        .then(response => response.json())
        .then(data => {
            if(data.status === "success") resolve(data.data);
            else reject(new Error(data.message));
        })
        .catch(err => { reject(new Error(err.message)) })
    })
}
