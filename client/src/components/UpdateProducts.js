import React, { useEffect, useRef, useState } from 'react';
import { useParams } from 'react-router';
import Form from './Form';

import createrAlert from '../util/alert';
import { getProduct, updateProduct } from '../requests/products';

import "../css/UpdateProduct.css";


function UpdateProduct() {
    const [product, setProduct] = useState({});
    const [state, setState] = useState(0);
    const [error, setError] = useState(null);

    const { id } = useParams();
    const containerRef = useRef();

    function handleSubmit(e, refs) {
        e.preventDefault();
        const formData = new FormData(e.currentTarget);
        updateProduct(id, formData)
        .then(data => {
            for(const refkey in refs) {
                refs[refkey].current.value = "";
            }
            createrAlert(containerRef, "El producto se actualizo satisfactoriamente", 2000, "success");
        })
        .catch(err => {
            createrAlert(containerRef, err.message, 2000, "danger");
        })
    }

    useEffect(() => {
        getProduct(id)
        .then(data => {
            setProduct(data);
            setState(1);
        })
        .catch(err => {
            setError(err.message);
            setState(-1);
        });
    }, []);

    return (
            <>
            {
                state === 1 ?
                (
                    <div className="update-product" ref={containerRef}>
                        <Form handleSubmit={ handleSubmit } initValues={product}/>;
                    </div>
                )
                : state === 0 ? 
                        <h1 className="message">Cargando</h1> 
                    : 
                        <h1 className="message">Error: {error}</h1>
            }
            </>
    )
}

export default UpdateProduct;