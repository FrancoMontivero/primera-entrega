import React from 'react';


function Card({ id, img, name, price, stock, isAdmin, children }) {
    return (
        <div className="card">
            <img src={img} alt={name} className="card__img"/>
            <span className="card__price">{price}{""}</span>
            <h5 className="card__title">{name}{""}</h5> 
            <span className="card__stock">{stock} unidades disponibles</span>
            {isAdmin && <a href={`/productos/${id}`} className="card__btn-edit">Editar</a> }
            {children}{""}
        </div>
    )
}

export default Card;