import React, { useContext, useRef } from 'react';

import Card from './Card';
import CartContext from '../contexts/CartContext';
import { addProductInCart, deleteProductInCart } from '../requests/cart';

import createrAlert from '../util/alert';

import "../css/Cart.css";

function Cart({isAdmin}) {
    const { cart, setCart } = useContext(CartContext);
    const refId = useRef();
    const refCart = useRef();

    function onSubmit(e) {
        e.preventDefault();
        addProductInCart(refId.current.value)
        .then(newProduct => {
            refId.current.value = ""
            console.log(cart);
            console.log(newProduct);
            createrAlert(refCart, "El producto se agrego al carrito", 2000, "success");
            setCart([...cart, newProduct]);
        })
        .catch(err => {
            createrAlert(refCart, err.message, 2000, "danger");
        })
    }

    function deleteProduct(id) {
        deleteProductInCart(id)
        .then(data => {
            createrAlert(refCart, "El producto se elimino del carrito", 2000, "success");
            let auxCart = cart;
            auxCart.splice(auxCart.findIndex(e => e.id === id), 1);
            setCart(auxCart)
        })
        .catch(err => {
            createrAlert(refCart, err.message, 2000, "danger");
        })
    }

    return (
        <div className="cart" ref={refCart}>      
            {cart instanceof Array  && cart.length 
                ?
                    <div className="contaienr-products">
                        {cart.map((e, i)=> (
                            <Card key={i} id={e.id} img={e.img} name={e.name} price={e.price} stock={e.stock} isAdmin={isAdmin}>
                                <button 
                                    className="card__btn-delete-cart"
                                    onClick={ () => { deleteProduct(e.id) } }
                                >
                                    Eliminar del carrito
                                </button>
                            </Card>
                        ))}{""}
                    </div>
                :
                    <h1 className="message">No hay productos en el carrito</h1>
            }
            <form onSubmit={ onSubmit } className="form-add-product-cart">
                <h2 className="form-add-product-cart__title">Agregar productos al carrito</h2>
                <input 
                    type="number" 
                    name="id" 
                    id="id" 
                    ref={refId} 
                    required 
                    className="form-add-product-cart__input" 
                    placeholder="Id del produto para agregar al carrito"
                />
                <button type="submit" className="form-add-product-cart__btn-submit">
                    Cargar al carrito
                </button>
            </form>  
        </div>
    );
}

export default Cart;