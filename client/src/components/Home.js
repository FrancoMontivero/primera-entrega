import React, { useEffect, useState } from 'react';
import { getProducts } from '../requests/products';
import Card from './Card';

import "../css/Home.css";

function Home({ isAdmin }) {
    const [products, setProducts] = useState([]);
    const [state, setState] = useState(0);
    const [error, setError] = useState(null);

    useEffect(() => {
        getProducts()
        .then(data => {
            setState(1);
            setProducts(data);
        })
        .catch(err => {
            setState(-1);
            setError(err.message);
        });
    }, []);

    return (
        <>
        {
            state === 1 ?
            (
                <div className="home">
                    {products.length ? products.map((e, i) => (
                        <Card key={i} id={e.id} img={e.img} name={e.name} price={e.price} stock={e.stock} isAdmin={ isAdmin }/>
                    )
                    ) : <h1 className="message">No hay productos cargados</h1> } 
                </div>
            )
            : state === 0 ? 
                    <h1 className="message">Cargando</h1> 
                : 
                    <h1 className="message">Error: {error}</h1>
        }
        </>
    )
}

export default Home;