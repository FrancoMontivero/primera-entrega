import React, { useEffect, useRef } from 'react';


function Form({ initValues, handleSubmit }) {
    const refs = {
        name: useRef(),
        description: useRef(),
        code: useRef(),
        img: useRef(),
        price: useRef(),
        stock: useRef(),
    };
    
    useEffect(() => {
        console.log("initValues");
        console.log(initValues);
        if(initValues) {
            for(const refkey in refs) {
                refs[refkey].current.value = initValues[refkey];
            }
        }
    }, []);

    return (
        <>
        <form onSubmit={ event => { handleSubmit(event, refs)} } className="form">
            <label htmlFor="name">Nombre</label>
            <input type="text" name="name" id="name" required ref={refs.name}/>
            <label htmlFor="code">Codigo</label>
            <input type="number" name="code" id="code" required ref={refs.code}/>
            <label htmlFor="img">URL imagen</label>
            <input type="text" name="img" id="img" required ref={refs.img}/>
            <label htmlFor="price">Precio</label>
            <input type="number" name="price" id="price" required ref={refs.price}/>
            <label htmlFor="stock">Stock</label>
            <input type="number" name="stock" id="stock" required ref={refs.stock}/>
            <label htmlFor="description">Descripcion</label>
            <textarea 
                name="description" 
                id="description" 
                cols="30" 
                rows="6"
                required 
                ref={refs.description}
            >
            </textarea>
            <button type="submit">Cargar</button>
        </form>
        </>
    )
}

export default Form;
