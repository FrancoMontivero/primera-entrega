import React, { useRef } from 'react';

import Form from './Form';
import createrAlert from '../util/alert';

import { addProduct } from '../requests/products';


import "../css/LoadProduct.css";


function LoadProduct() {
    const containerRef = useRef();

    function handleSubmit(e, refs) {
        e.preventDefault();
        const formData = new FormData(e.currentTarget);
        addProduct(formData)
        .then(data => {
            for(const refkey in refs) {
                refs[refkey].current.value = "";
            }
            createrAlert(containerRef, "El producto se cargo satisfactoriamente", 2000, "success");
        })
        .catch(err => {
            createrAlert(containerRef, err.message, 2000, "danger");
        })
    }

    return (
    <div className="load-product" ref={containerRef}>
        <Form handleSubmit={ handleSubmit }/>;
    </div>
    )
    
}

export default LoadProduct;