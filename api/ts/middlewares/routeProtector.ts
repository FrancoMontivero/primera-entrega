export let isAdmin: boolean = true;

export function routeProtector(nameRoute: string, nameMethod: string): (req: any, res: any, next: any)  => void{
    return function(req: any, res: any, next: any) {
        if(isAdmin) next();
        else res.json({error: -1, descripcion: `ruta ${nameRoute} metodo ${nameMethod} no autorizada`});
    }
}