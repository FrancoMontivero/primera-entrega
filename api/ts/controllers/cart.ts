import fs from 'fs';
import path from 'path';
import { Product, getProduct } from './products';

interface Cart {
    id: number,
    timestamp: number,
    products: Array<Product>
}

export function initFileCart() {
    fs.writeFileSync(path.resolve(__dirname, '../', 'carts.json'), 
                     JSON.stringify([{id: 1, timestamp: Date.now(), products: []}]), 
                     {flag: 'w+'});
};

export function getProductsCart(idCart: number, idProduct?: number) {
    if(Number.isNaN(Number(idCart))) 
        return {"status": "fail", "message": "El id del carrito debe ser un caracter numerico"};
    else if(typeof idCart !== "number") idCart = parseInt(idCart);
    if(idProduct){
        if(Number.isNaN(Number(idProduct))) 
            return {"status": "fail", "message": "El id del producto debe ser un caracter numerico"};
        else if(typeof idProduct !== "number") idProduct = parseInt(idProduct);
    }
    try {
        const response = fs.readFileSync(path.resolve(__dirname, '../', 'carts.json')).toString();
        const carts: Array<Cart> = JSON.parse(response || "[]");
        const cart: Cart | undefined = carts.find(e => e.id === idCart);
        if(cart) {
            if(idProduct) {
                const index: number = cart.products.findIndex(e => e.id === idProduct);
                if(index !== -1) return {"status": "success", "data": cart.products[index]};
                else 
                    return {
                        "status": "fail", 
                        "message": `En el carrito con id ${idCart} No hay un producto con el id ${idProduct}`
                    };
            } else return {"status": "success", "data": cart.products};
        }
        return {"status": "fail", "message": `No existe un carrito con el id ${idCart}`};
    } catch(err) { throw err; }
};

export function addProductCart(idCart: number, idProduct: number) {
    if(Number.isNaN(Number(idCart))) 
        return {"status": "fail", "message": "El id del carrito debe ser un caracter numerico"};
    else if(typeof idCart !== "number") idCart = parseInt(idCart);
    if(idProduct){
        if(Number.isNaN(Number(idProduct))) 
            return {"status": "fail", "message": "El id del producto debe ser un caracter numerico"};
        else if(typeof idProduct !== "number") idProduct = parseInt(idProduct);
    }
    try {
        const response = fs.readFileSync(path.resolve(__dirname, '../', 'carts.json')).toString();
        const carts: Array<Cart> = JSON.parse(response);
        const indexCart: number = carts.findIndex(e => e.id === idCart);
        if(indexCart !== -1) {
            const newProduct = getProduct(idProduct);
            if(newProduct.status !== "success") return newProduct;
            carts[indexCart].products.push(newProduct.data);
            fs.writeFileSync(path.resolve(__dirname, '../', 'carts.json'), JSON.stringify(carts));
            return {"status": "success", "data": newProduct.data};
        }
        return {"status": "fail", "message": `No existe un carrito con el id ${idCart}`};
    } catch (err) { throw err; }
}

export function removeProductCart(idCart: number, idProduct: number) {
    if(Number.isNaN(Number(idCart))) 
        return {"status": "fail", "message": "El id del carrito debe ser un caracter numerico"};
    else if(typeof idCart !== "number") idCart = parseInt(idCart);
    if(idProduct){
        if(Number.isNaN(Number(idProduct))) 
            return {"status": "fail", "message": "El id del producto debe ser un caracter numerico"};
        else if(typeof idProduct !== "number") idProduct = parseInt(idProduct);
    }
    try {
        const response = fs.readFileSync(path.resolve(__dirname, '../', 'carts.json')).toString();
        const carts: Array<Cart> = JSON.parse(response);
        const indexCart: number = carts.findIndex(e => e.id === idCart);
        if(indexCart !== -1) {
            const indexProduct: number = carts[indexCart].products.findIndex(e => e.id === idProduct);
            if(indexProduct !== -1) {
                carts[indexCart].products.splice(indexProduct, 1);
                fs.writeFileSync(path.resolve(__dirname, '../', 'carts.json'), JSON.stringify(carts));
                return {"status": "success", "data": null};
            }
            return {
                "status": "fail", 
                "message": `No existe un producto en el carrito ${idCart} con el id ${idProduct}`
            };
        }
        return {"status": "fail", "message": `No existe un carrito con el id ${idCart}`};
    } catch (err) { throw err; }
}