import fs from 'fs';
import path from 'path';

export interface Product {
    id: number,
    timestamp: number,
    name: string,
    description: string,
    code: string,
    img: string,
    price: number,
    stock: number
}

let currentId: number = 0;

function loadProducts(products: Array<Product>) {
    fs.writeFileSync(path.resolve(__dirname, '../', 'products.json'), JSON.stringify(products));
}

export function initFileProducts() {
    fs.writeFileSync(path.resolve(__dirname, '../', 'products.json'), JSON.stringify([]), {flag: "w+"});
}

export function getProducts(): {"status": "success", data: Array<Product>} {
    try {
        const response = fs.readFileSync(path.resolve(__dirname, '../', 'products.json')).toString();
        return { "status": "success", data:  response ? JSON.parse(response) : [] }
    } catch (err) { throw err };
}

export function getProduct(id: number | string): {"status": "fail", "message": string} |
                {"status": "success", data: Product} {
    if(Number.isNaN(Number(id))) 
        return {"status": "fail", "message": "El id del producto debe ser un caracter numerico"};
    else if(typeof id !== "number") id = parseInt(id);
    try {
        const product: Product | undefined = getProducts().data.find(e => id === e.id);
        if(product) return {"status": "success", "data": product};
        else return {"status": "fail", "message": "Producto no encontrado"};
    } catch (err) { throw err; }
}

export function addProduct(name: string, description: string, code: string, img: string, 
                price: number | string, stock: number | string): {"status": "fail", "message": string} |
                {"status": "success", data: Product} {
    if(!(name && description && code && img && price && stock)) {
        return {"status": "fail", "message": "Hay parametros vacios o indefinidos"};
    }
    if(Number.isNaN(Number(price))) 
        return {"status": "fail", "message": "El precio no puede contener caracteres no numericos"};
    else if(typeof price !== "number") price = parseFloat(price)
    if(Number.isNaN(Number(stock)))
        return {"status": "fail", "message": "El stock no puede contener caracteres no numericos"};
    else if(typeof stock !== "number") stock = parseInt(stock);

    const newProduct = { id: ++currentId, timestamp: Date.now(), name, description, code, img, price, stock};
    try {
        const products = getProducts().data;
        products.push(newProduct);
        loadProducts(products);
        return {"status": "success", "data": newProduct};
    } catch (err) { throw err };
}

export function updateProduct(id: number | string, name: string, description: string, 
                code: string, img: string, price: number | string, stock: number | string): 
                {"status": "fail", "message": string} | {"status": "success", data: Product} {
    if(!(name && description && code && img && price && stock)) {
        return {"status": "fail", "message": "Hay parametros vacios o indefinidos"};
    }
    if(Number.isNaN(Number(id))) 
        return {"status": "fail", "message": "El id no puede contener caracteres no numericos"};
    else if(typeof id !== "number") id = parseInt(id)
    if(Number.isNaN(Number(price)))
        return {"status": "fail", "message": "El precio no puede contener caracteres no numericos"};
    else if(typeof price !== "number") price = parseFloat(price)
    if(Number.isNaN(Number(stock)))
    return {"status": "fail", "message": "El stock no puede contener caracteres no numericos"};
    else if(typeof stock !== "number") stock = parseInt(stock)

    try {
        const products = getProducts().data;
        const index: number = products.findIndex(e => e.id === id);
    
        if(index !== -1) {
            products[index] = { id, timestamp: Date.now(), name, description, code, img, price, stock};
            loadProducts(products);
            return {"status": "success", data: products[index]};
        }
        return {"status": "fail", "message": `No existe un producto con el id ${id}`};
    } catch (err) { throw err };
}

export function removeProduct(id: number | string): {"status": "fail", "message": string} | 
                {"status": "success", data: Product} {
    if(Number.isNaN(Number(id))) 
    return {"status": "fail", "message": "El id del producto debe ser un caracter numerico"};
    else if(typeof id !== "number") id = parseInt(id);
    try {
        const products: Array<Product> = getProducts().data;
        const index: number = products.findIndex(e => e.id === id);
        if(index !== -1) {
            const aux :Product = products[index];
            products.splice(index, 1);
            loadProducts(products);
            return {"status": "success", data: aux};
        }
        return {"status": "fail", "message": `No existe un producto con el id ${id}`};
    } catch (err) { throw err };
}