import {
    getProductsCart,
    addProductCart,
    removeProductCart
} from '../controllers/cart';

import express from 'express';

const routerCart = express.Router();

routerCart.get('/listar', (req, res) => {
    try { res.json(getProductsCart(1)); }
    catch (err) { res.json({"status": "error", "message": err.message}) };
})

routerCart.get('/listar/:id', (req, res) => {
    try { res.json(getProductsCart(1, parseInt(req.params.id))); }
    catch (err) { res.json({"status": "error", "message": err.message}); };
});

routerCart.post('/agregar/:id', (req, res) => {
    console.log(req.params.id);
    try { res.json(addProductCart(1, parseInt(req.params.id))) }
    catch (err) { res.json({"status": "error", "message": err.message}); };
});

routerCart.delete('/borrar/:id', (req, res) => {
    try { res.json(removeProductCart(1, parseInt(req.params.id))) }
    catch (err) { res.json({"status": "error", "message": err.message}); };
});

export default routerCart;