import express from 'express';
import routerProducts from './products';
import routerCart from './cart';
import { initFileCart } from '../controllers/cart';

const router = express.Router();

initFileCart();
router.use('/productos', routerProducts);
router.use('/carrito', routerCart);

export default router;
