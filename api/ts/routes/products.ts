import express from 'express';
import { routeProtector } from '../middlewares/routeProtector';

import {
	getProducts,
	getProduct, 
	addProduct, 
	updateProduct, 
	removeProduct,
	initFileProducts
} from '../controllers/products';

const routerProducts = express.Router();

initFileProducts();

routerProducts.get('/listar', (req, res) => {
	try { res.json(getProducts()); }
	catch (err) { res.json({"status": "error", "message": err.message}) }
})

routerProducts.get('/listar/:id', (req, res) => {
	const id: number = parseInt(req.params.id);
	try { res.json(getProduct(id)); }
	catch (err) { res.json({"status": "error", "message": err.message}); };
})

routerProducts.post('/guardar', routeProtector('productos/guardar', 'POST'), (req, res) => {
	const { name, description, code, img, price, stock  } = req.body;
	try { res.json(addProduct(name, description, code, img, price, stock)); }
	catch (err) { res.json({"status": "error", "message": err.message}); };
})

routerProducts.put('/actualizar/:id', routeProtector('productos/actualizar/:id', 'PUT'), (req, res) => {
	const { name, description, code, img, price, stock  } = req.body;
	const id: number = parseInt(req.params.id);
	try { res.json(updateProduct(id, name, description, code, img, price, stock)); }
	catch (err) { res.json({"status": "error", "message": err.message}); };
})

routerProducts.delete('/borrar/:id', routeProtector('productos/delete', 'DELETE'), (req, res) => {
	const id: number = parseInt(req.params.id);
	try { res.json(removeProduct(id)); }
	catch (err) { res.json({"status": "error", "message": err.message}); };
})

export default routerProducts;