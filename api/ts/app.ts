import express from 'express';

import router from './routes';

import cors from './middlewares/cors';
import multer from './middlewares/multer';
import morgan from './middlewares/morgan';
import errorHandler from './middlewares/errorHandler';

import { SHOW_REQUEST } from './config';

const app = express();

app.use(express.json());
app.use(multer.array('development'));
app.use(express.urlencoded({ extended : true }));

if(SHOW_REQUEST === 'enabled') app.use(morgan); 

app.use(cors);

app.use(router);
app.use(errorHandler);

export default app;