"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.routeProtector = exports.isAdmin = void 0;
exports.isAdmin = true;
function routeProtector(nameRoute, nameMethod) {
    return function (req, res, next) {
        if (exports.isAdmin)
            next();
        else
            res.json({ error: -1, descripcion: `ruta ${nameRoute} metodo ${nameMethod} no autorizada` });
    };
}
exports.routeProtector = routeProtector;
