"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const routes_1 = __importDefault(require("./routes"));
const cors_1 = __importDefault(require("./middlewares/cors"));
const multer_1 = __importDefault(require("./middlewares/multer"));
const morgan_1 = __importDefault(require("./middlewares/morgan"));
const errorHandler_1 = __importDefault(require("./middlewares/errorHandler"));
const config_1 = require("./config");
const app = express_1.default();
app.use(express_1.default.json());
app.use(multer_1.default.array('development'));
app.use(express_1.default.urlencoded({ extended: true }));
if (config_1.SHOW_REQUEST === 'enabled')
    app.use(morgan_1.default);
app.use(cors_1.default);
app.use(routes_1.default);
app.use(errorHandler_1.default);
exports.default = app;
