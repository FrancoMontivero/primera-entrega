"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const products_1 = __importDefault(require("./products"));
const cart_1 = __importDefault(require("./cart"));
const cart_2 = require("../controllers/cart");
const router = express_1.default.Router();
cart_2.initFileCart();
router.use('/productos', products_1.default);
router.use('/carrito', cart_1.default);
exports.default = router;
