"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cart_1 = require("../controllers/cart");
const express_1 = __importDefault(require("express"));
const routerCart = express_1.default.Router();
routerCart.get('/listar', (req, res) => {
    try {
        res.json(cart_1.getProductsCart(1));
    }
    catch (err) {
        res.json({ "status": "error", "message": err.message });
    }
    ;
});
routerCart.get('/listar/:id', (req, res) => {
    try {
        res.json(cart_1.getProductsCart(1, parseInt(req.params.id)));
    }
    catch (err) {
        res.json({ "status": "error", "message": err.message });
    }
    ;
});
routerCart.post('/agregar/:id', (req, res) => {
    console.log(req.params.id);
    try {
        res.json(cart_1.addProductCart(1, parseInt(req.params.id)));
    }
    catch (err) {
        res.json({ "status": "error", "message": err.message });
    }
    ;
});
routerCart.delete('/borrar/:id', (req, res) => {
    try {
        res.json(cart_1.removeProductCart(1, parseInt(req.params.id)));
    }
    catch (err) {
        res.json({ "status": "error", "message": err.message });
    }
    ;
});
exports.default = routerCart;
