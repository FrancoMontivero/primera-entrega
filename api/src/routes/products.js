"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const routeProtector_1 = require("../middlewares/routeProtector");
const products_1 = require("../controllers/products");
const routerProducts = express_1.default.Router();
products_1.initFileProducts();
routerProducts.get('/listar', (req, res) => {
    try {
        res.json(products_1.getProducts());
    }
    catch (err) {
        res.json({ "status": "error", "message": err.message });
    }
});
routerProducts.get('/listar/:id', (req, res) => {
    const id = parseInt(req.params.id);
    try {
        res.json(products_1.getProduct(id));
    }
    catch (err) {
        res.json({ "status": "error", "message": err.message });
    }
    ;
});
routerProducts.post('/guardar', routeProtector_1.routeProtector('productos/guardar', 'POST'), (req, res) => {
    const { name, description, code, img, price, stock } = req.body;
    try {
        res.json(products_1.addProduct(name, description, code, img, price, stock));
    }
    catch (err) {
        res.json({ "status": "error", "message": err.message });
    }
    ;
});
routerProducts.put('/actualizar/:id', routeProtector_1.routeProtector('productos/actualizar/:id', 'PUT'), (req, res) => {
    const { name, description, code, img, price, stock } = req.body;
    const id = parseInt(req.params.id);
    try {
        res.json(products_1.updateProduct(id, name, description, code, img, price, stock));
    }
    catch (err) {
        res.json({ "status": "error", "message": err.message });
    }
    ;
});
routerProducts.delete('/borrar/:id', routeProtector_1.routeProtector('productos/delete', 'DELETE'), (req, res) => {
    const id = parseInt(req.params.id);
    try {
        res.json(products_1.removeProduct(id));
    }
    catch (err) {
        res.json({ "status": "error", "message": err.message });
    }
    ;
});
exports.default = routerProducts;
