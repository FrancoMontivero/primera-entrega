"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SHOW_REQUEST = exports.ORIGIN_CLIENT = exports.PORT = void 0;
const dotenv_1 = __importDefault(require("dotenv"));
const path_1 = __importDefault(require("path"));
dotenv_1.default.config({ path: path_1.default.resolve(__dirname, '../', process.env.NODE_ENV + '.env') });
exports.PORT = process.env.PORT ? parseInt(process.env.PORT) : 8080;
exports.ORIGIN_CLIENT = process.env.ORIGIN_CLIENT || 'http://localhost:3000';
exports.SHOW_REQUEST = process.env.SHOW_REQUEST || 'enabled';
