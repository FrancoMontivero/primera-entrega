"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.removeProduct = exports.updateProduct = exports.addProduct = exports.getProduct = exports.getProducts = exports.initFileProducts = void 0;
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
let currentId = 0;
function loadProducts(products) {
    fs_1.default.writeFileSync(path_1.default.resolve(__dirname, '../', 'products.json'), JSON.stringify(products));
}
function initFileProducts() {
    fs_1.default.writeFileSync(path_1.default.resolve(__dirname, '../', 'products.json'), JSON.stringify([]), { flag: "w+" });
}
exports.initFileProducts = initFileProducts;
function getProducts() {
    try {
        const response = fs_1.default.readFileSync(path_1.default.resolve(__dirname, '../', 'products.json')).toString();
        return { "status": "success", data: response ? JSON.parse(response) : [] };
    }
    catch (err) {
        throw err;
    }
    ;
}
exports.getProducts = getProducts;
function getProduct(id) {
    if (Number.isNaN(Number(id)))
        return { "status": "fail", "message": "El id del producto debe ser un caracter numerico" };
    else if (typeof id !== "number")
        id = parseInt(id);
    try {
        const product = getProducts().data.find(e => id === e.id);
        if (product)
            return { "status": "success", "data": product };
        else
            return { "status": "fail", "message": "Producto no encontrado" };
    }
    catch (err) {
        throw err;
    }
}
exports.getProduct = getProduct;
function addProduct(name, description, code, img, price, stock) {
    if (!(name && description && code && img && price && stock)) {
        return { "status": "fail", "message": "Hay parametros vacios o indefinidos" };
    }
    if (Number.isNaN(Number(price)))
        return { "status": "fail", "message": "El precio no puede contener caracteres no numericos" };
    else if (typeof price !== "number")
        price = parseFloat(price);
    if (Number.isNaN(Number(stock)))
        return { "status": "fail", "message": "El stock no puede contener caracteres no numericos" };
    else if (typeof stock !== "number")
        stock = parseInt(stock);
    const newProduct = { id: ++currentId, timestamp: Date.now(), name, description, code, img, price, stock };
    try {
        const products = getProducts().data;
        products.push(newProduct);
        loadProducts(products);
        return { "status": "success", "data": newProduct };
    }
    catch (err) {
        throw err;
    }
    ;
}
exports.addProduct = addProduct;
function updateProduct(id, name, description, code, img, price, stock) {
    if (!(name && description && code && img && price && stock)) {
        return { "status": "fail", "message": "Hay parametros vacios o indefinidos" };
    }
    if (Number.isNaN(Number(id)))
        return { "status": "fail", "message": "El id no puede contener caracteres no numericos" };
    else if (typeof id !== "number")
        id = parseInt(id);
    if (Number.isNaN(Number(price)))
        return { "status": "fail", "message": "El precio no puede contener caracteres no numericos" };
    else if (typeof price !== "number")
        price = parseFloat(price);
    if (Number.isNaN(Number(stock)))
        return { "status": "fail", "message": "El stock no puede contener caracteres no numericos" };
    else if (typeof stock !== "number")
        stock = parseInt(stock);
    try {
        const products = getProducts().data;
        const index = products.findIndex(e => e.id === id);
        if (index !== -1) {
            products[index] = { id, timestamp: Date.now(), name, description, code, img, price, stock };
            loadProducts(products);
            return { "status": "success", data: products[index] };
        }
        return { "status": "fail", "message": `No existe un producto con el id ${id}` };
    }
    catch (err) {
        throw err;
    }
    ;
}
exports.updateProduct = updateProduct;
function removeProduct(id) {
    if (Number.isNaN(Number(id)))
        return { "status": "fail", "message": "El id del producto debe ser un caracter numerico" };
    else if (typeof id !== "number")
        id = parseInt(id);
    try {
        const products = getProducts().data;
        const index = products.findIndex(e => e.id === id);
        if (index !== -1) {
            const aux = products[index];
            products.splice(index, 1);
            loadProducts(products);
            return { "status": "success", data: aux };
        }
        return { "status": "fail", "message": `No existe un producto con el id ${id}` };
    }
    catch (err) {
        throw err;
    }
    ;
}
exports.removeProduct = removeProduct;
