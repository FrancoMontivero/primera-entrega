"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.removeProductCart = exports.addProductCart = exports.getProductsCart = exports.initFileCart = void 0;
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const products_1 = require("./products");
function initFileCart() {
    fs_1.default.writeFileSync(path_1.default.resolve(__dirname, '../', 'carts.json'), JSON.stringify([{ id: 1, timestamp: Date.now(), products: [] }]), { flag: 'w+' });
}
exports.initFileCart = initFileCart;
;
function getProductsCart(idCart, idProduct) {
    if (Number.isNaN(Number(idCart)))
        return { "status": "fail", "message": "El id del carrito debe ser un caracter numerico" };
    else if (typeof idCart !== "number")
        idCart = parseInt(idCart);
    if (idProduct) {
        if (Number.isNaN(Number(idProduct)))
            return { "status": "fail", "message": "El id del producto debe ser un caracter numerico" };
        else if (typeof idProduct !== "number")
            idProduct = parseInt(idProduct);
    }
    try {
        const response = fs_1.default.readFileSync(path_1.default.resolve(__dirname, '../', 'carts.json')).toString();
        const carts = JSON.parse(response || "[]");
        const cart = carts.find(e => e.id === idCart);
        if (cart) {
            if (idProduct) {
                const index = cart.products.findIndex(e => e.id === idProduct);
                if (index !== -1)
                    return { "status": "success", "data": cart.products[index] };
                else
                    return {
                        "status": "fail",
                        "message": `En el carrito con id ${idCart} No hay un producto con el id ${idProduct}`
                    };
            }
            else
                return { "status": "success", "data": cart.products };
        }
        return { "status": "fail", "message": `No existe un carrito con el id ${idCart}` };
    }
    catch (err) {
        throw err;
    }
}
exports.getProductsCart = getProductsCart;
;
function addProductCart(idCart, idProduct) {
    if (Number.isNaN(Number(idCart)))
        return { "status": "fail", "message": "El id del carrito debe ser un caracter numerico" };
    else if (typeof idCart !== "number")
        idCart = parseInt(idCart);
    if (idProduct) {
        if (Number.isNaN(Number(idProduct)))
            return { "status": "fail", "message": "El id del producto debe ser un caracter numerico" };
        else if (typeof idProduct !== "number")
            idProduct = parseInt(idProduct);
    }
    try {
        const response = fs_1.default.readFileSync(path_1.default.resolve(__dirname, '../', 'carts.json')).toString();
        const carts = JSON.parse(response);
        const indexCart = carts.findIndex(e => e.id === idCart);
        if (indexCart !== -1) {
            const newProduct = products_1.getProduct(idProduct);
            if (newProduct.status !== "success")
                return newProduct;
            carts[indexCart].products.push(newProduct.data);
            fs_1.default.writeFileSync(path_1.default.resolve(__dirname, '../', 'carts.json'), JSON.stringify(carts));
            return { "status": "success", "data": newProduct.data };
        }
        return { "status": "fail", "message": `No existe un carrito con el id ${idCart}` };
    }
    catch (err) {
        throw err;
    }
}
exports.addProductCart = addProductCart;
function removeProductCart(idCart, idProduct) {
    if (Number.isNaN(Number(idCart)))
        return { "status": "fail", "message": "El id del carrito debe ser un caracter numerico" };
    else if (typeof idCart !== "number")
        idCart = parseInt(idCart);
    if (idProduct) {
        if (Number.isNaN(Number(idProduct)))
            return { "status": "fail", "message": "El id del producto debe ser un caracter numerico" };
        else if (typeof idProduct !== "number")
            idProduct = parseInt(idProduct);
    }
    try {
        const response = fs_1.default.readFileSync(path_1.default.resolve(__dirname, '../', 'carts.json')).toString();
        const carts = JSON.parse(response);
        const indexCart = carts.findIndex(e => e.id === idCart);
        if (indexCart !== -1) {
            const indexProduct = carts[indexCart].products.findIndex(e => e.id === idProduct);
            if (indexProduct !== -1) {
                carts[indexCart].products.splice(indexProduct, 1);
                fs_1.default.writeFileSync(path_1.default.resolve(__dirname, '../', 'carts.json'), JSON.stringify(carts));
                return { "status": "success", "data": null };
            }
            return {
                "status": "fail",
                "message": `No existe un producto en el carrito ${idCart} con el id ${idProduct}`
            };
        }
        return { "status": "fail", "message": `No existe un carrito con el id ${idCart}` };
    }
    catch (err) {
        throw err;
    }
}
exports.removeProductCart = removeProductCart;
